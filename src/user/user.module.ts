import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {UsersEntity} from "../entities/users.entity";
import {DoctorModule} from "../doctor/doctor.module";
import {MeetingEntity} from "../entities/meeting.entity";

@Module({
  imports:[TypeOrmModule.forFeature([UsersEntity,MeetingEntity]),DoctorModule],
  providers: [UserService],
  controllers: [UserController],
  exports:[UserService]
})
export class UserModule {}
