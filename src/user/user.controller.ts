import {Body, Controller, HttpCode, Post, Req} from '@nestjs/common';
import {CreateUserDto} from "../common/dto/create-user-dto";
import {ApiBody} from "@nestjs/swagger";
import {UserService} from "./user.service";
import {CreateMeetingDto} from "../common/dto/create-meeting-dto";

@Controller('user')
export class UserController {
    constructor(private readonly userService:UserService) {
    }

    @Post(`create-user`)
    @ApiBody({ type: CreateUserDto })
    @HttpCode(201)
    async createUser(@Body() body:CreateUserDto){
        await this.userService.createUser(body.phone,body.email,body.password,body.name)
    }

    @Post(`create-meeting`)
    @ApiBody({ type: CreateMeetingDto })
    @HttpCode(201)
    async createMeetingWithDoctor(@Body() body:CreateMeetingDto){
        await this.userService.createMeeting(body.userId,body.doctorId,body.date)
    }
}
