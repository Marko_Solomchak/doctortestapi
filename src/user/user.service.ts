import { Injectable } from '@nestjs/common';
import {UsersEntity} from "../entities/users.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {hash} from "bcrypt"
import {DoctorService} from "../doctor/doctor.service";
import {MeetingEntity} from "../entities/meeting.entity";

@Injectable()
export class UserService {
    constructor(@InjectRepository(UsersEntity) private readonly userEntity: Repository<UsersEntity>,
                @InjectRepository(MeetingEntity) private readonly meetingEntity: Repository<MeetingEntity>,
               private readonly doctorService:DoctorService) {
    }

    async createUser(phone,email,password,name){
        const candidate = await this.userEntity.findOne({where:{email}})

        if(candidate) throw new Error(`User already exists`)

        const hashPass = await hash(password,3,(e) => {
            if(e)
                console.log(e)
        })

        await this.userEntity.save({email,password:hashPass,phone,name})
    }

    async createMeeting(userId,doctorId,date){
        const user = await this.userEntity.findOne({where:{id:userId}})

        if(!user) throw Error(`User not found`)

        const doctor = await this.doctorService.findDoctorById(doctorId)

        if(!doctor) throw Error(`Doctor not found`)
        const freeDate = []
        doctor.slots.map(async(e,i) => {
            if(e === date){
                doctor.slots.splice(i,1)
                freeDate.push(e)
                await this.doctorService.saveDoctor(doctor)
            }
            return false
        })

        if(freeDate.length <= 0) throw Error(`Time is not free`)

        const meetings = await this.meetingEntity.findOne({where:{doctorId,date}})
        if(meetings) return Error(`Meeting is already exists`)

        await this.meetingEntity.save({doctorId,userId,date})
    }

    async findUserById(id){
        return await this.userEntity.findOne({where:{id}})
    }
}
