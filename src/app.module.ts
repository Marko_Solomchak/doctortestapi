import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { join } from 'path';
import { ConfigModule } from '@nestjs/config';
import { DatabaseConnectionModule } from './database-connection/database-connection.module';
import {DatabaseConnectionService} from "./database-connection/database-connection.service";
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { DoctorModule } from './doctor/doctor.module';
import {ScheduleModule} from "@nestjs/schedule";
import { ReminderModule } from './reminder/reminder.module';

@Module({
    imports: [ConfigModule.forRoot({
      envFilePath: join(__dirname, `../.env`),
      ignoreEnvFile: false,
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
        imports: [DatabaseConnectionModule],
        inject: [DatabaseConnectionService],
        useFactory: async (
            databaseConnectionService: DatabaseConnectionService
        ) => databaseConnectionService.getPostgresConfig(),
    }),
    ScheduleModule.forRoot(),
    UserModule,
    DoctorModule,
    ReminderModule,
    ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
