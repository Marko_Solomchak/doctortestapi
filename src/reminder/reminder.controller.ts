import {Controller, Post} from '@nestjs/common';
import {ReminderService} from "./reminder.service";
import {Cron, CronExpression} from "@nestjs/schedule";

@Controller('reminder')
export class ReminderController {
    constructor(private readonly reminderService:ReminderService) {
    }

    @Post(`post`)
    @Cron(CronExpression.EVERY_2_HOURS)
    async some(){
        return await this.reminderService.remindForUser(1,1)
    }
}
