import { Injectable } from '@nestjs/common';
import {Cron, CronExpression} from "@nestjs/schedule";
import {UserService} from "../user/user.service";
import {InjectRepository} from "@nestjs/typeorm";
import {MeetingEntity} from "../entities/meeting.entity";
import {Repository} from "typeorm";
import {CronJob} from "cron";
import {logger} from "../common/logger/logger";
import {DoctorService} from "../doctor/doctor.service";

@Injectable()
export class ReminderService {
    constructor(private readonly userService:UserService ,
                private readonly doctorService:DoctorService,
                @InjectRepository(MeetingEntity) private readonly meetingEntity: Repository<MeetingEntity>) {
    }

    async remindForUser(id,doctor_id){
        const user = await this.userService.findUserById(id);

        if(!user) throw Error(`User not found`)

        const doctor = await this.doctorService.findDoctorById(doctor_id)

        if(!doctor) throw new Error(`Doctor not found`)
        const dateNow = new Date()

        const findRecord = await this.meetingEntity.findOne({where:{id}})


        if(Number(findRecord.date.getDay()) === 1){
            console.log(`| Привет ${user.name}! Напоминаем что вы записаны к ${doctor.name} завтра в ${findRecord.date}`)
            const job = new CronJob(`* * * * * *`, (() => {
                logger.info(`| Привет ${user.name}! Напоминаем что вы записаны к ${doctor.name} завтра в ${findRecord.date}`);
            }));
            job.start();
        }

        if(Number(findRecord.date.getHours()) - dateNow.getHours() <= 2){
            console.log(`${new Date()} | Привет ${user.name}! Вам через 2 часа к ${doctor.name} в ${findRecord.date}!`)
            const job = new CronJob(`* * * * * *`, (() => {
                logger.info(`${new Date()} | Привет ${user.name}! Вам через 2 часа к ${doctor.name} в ${findRecord.date}!`);
            }));
            job.start();
        }
    }
}
