import { Module } from '@nestjs/common';
import { ReminderService } from './reminder.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MeetingEntity} from "../entities/meeting.entity";
import { ReminderController } from './reminder.controller';
import {UserModule} from "../user/user.module";
import {DoctorModule} from "../doctor/doctor.module";

@Module({
  imports:[TypeOrmModule.forFeature([MeetingEntity]),UserModule,DoctorModule],
  providers: [ReminderService],
  controllers: [ReminderController]
})
export class ReminderModule {}
