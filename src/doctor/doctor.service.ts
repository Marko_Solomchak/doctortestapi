import { Injectable } from '@nestjs/common';
import { Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";
import {DoctorEntity} from "../entities/doctor.entity";
import {hash} from "bcrypt"
let DateGenerator = require('random-date-generator');

@Injectable()
export class DoctorService {
    constructor(@InjectRepository(DoctorEntity) private readonly doctorEntity: Repository<DoctorEntity>) {
    }

    async createDoctor(name,position,phone,password,email){
       const candidate = await this.doctorEntity.findOne({where:{email}})

        if(candidate) throw new Error(`User already exists`)

        const hashPass = await hash(password,3,(error) => {
            if(error)
                return error
        })

        const slots = [DateGenerator.getRandomDate(),DateGenerator.getRandomDate(),DateGenerator.getRandomDate()]

        await this.doctorEntity.save({email,password:hashPass,phone,name,position,slots})

    }

    async findDoctorById(id){
       return this.doctorEntity.findOne({where:{id}})
    }

    async saveDoctor(entity){
        return this.doctorEntity.save(entity)
    }
}
