import {Body, Controller, HttpCode, Post} from '@nestjs/common';
import {UserService} from "../user/user.service";
import {ApiBody} from "@nestjs/swagger";
import {CreateUserDto} from "../common/dto/create-user-dto";
import {DoctorService} from "./doctor.service";
import {CreateDoctorDto} from "../common/dto/create-doctor-dto";

@Controller('doctor')
export class DoctorController {
    constructor(private readonly doctorService:DoctorService) {
    }

    @Post(`create-doctor`)
    @ApiBody({ type: CreateDoctorDto })
    @HttpCode(201)

    async createDoctor(@Body() body:CreateDoctorDto){
        await this.doctorService.createDoctor(body.name,body.position,body.phone,body.password,body.email)
    }
}
