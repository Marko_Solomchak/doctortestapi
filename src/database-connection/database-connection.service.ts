import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import {UsersEntity} from "../entities/users.entity";
import {DoctorEntity} from "../entities/doctor.entity";
import {MeetingEntity} from "../entities/meeting.entity";

@Injectable()
export class DatabaseConnectionService {
    private readonly postgresConfig: TypeOrmModuleOptions;

    public constructor(private readonly configService: ConfigService) {
        this.postgresConfig = {
            type: "postgres",
            host: this.configService.get<string>(`POSTGRES_HOST`),
            port: this.configService.get<number>(`POSTGRES_PORT`),
            username: this.configService.get<string>(`POSTGRES_USERNAME`),
            password: this.configService.get<string>(`POSTGRES_PASSWORD`),
            database: this.configService.get<string>(`POSTGRES_DATABASE`),
            synchronize: true,
            retryAttempts: 3,

            entities: [UsersEntity,DoctorEntity,MeetingEntity],
            migrationsRun: false,
            // migrations: ["./migrations/*.ts"],

        };
    }

    public getPostgresConfig() {
        return this.postgresConfig;
    }
}